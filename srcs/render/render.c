#include "cub3d.h"

void	put_pixel_image(t_data data, int x, int y, int color)
{
	*(unsigned int *)
	(data.img_p + 
	(x * data.setup.size_line +
	 y * (data.setup.bpp / 8)))	= color;
}

void	render_background(t_data data)
{
	int x;
	int y;

	x = 0;
	while (x < HEIGHT)
	{
		y = 0;
		while (y < WIDTH)
		{
			if (x < HEIGHT / 2)
				put_pixel_image(data, x, y, data.setup.sky_color);
			else
				put_pixel_image(data, x, y, data.setup.floor_color);
			y++;
		}
		x++;
	}
}

void	init_render(t_render *render, int x)
{
}

void	render(t_data *data)
{
	static	t_render	render;
	int x;

	render.setup = &(data->setup);
	render.playr = &(data->player);
	x = 0;
	while (x < WIDTH)
	{
		render_background(*data);
		init_render(&render, x);
	}
	mlx_put_image_to_window(data->mlx, data->win, data->img, 0, 0);
	return ;
}