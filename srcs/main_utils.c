#include "cub3d.h"

int	check_file_name(char *file_name)
{
	int i;

	i = ft_strlen(file_name);
	if (i < 4)
		return (0);
	if (ft_strncmp(file_name + i - 4, ".cub", 4) != 0)
		return (0);
	return (1);
}

void	init_data_h(t_data *data)
{
	data->map = NULL;
	data->mlx = NULL;
	data->win = NULL;
	data->img = NULL;
	data->texture.north = NULL;
	data->texture.south = NULL;
	data->texture.est = NULL;
	data->texture.west = NULL;
	data->txtrs = malloc(sizeof(char *) * 4);
	if (data->txtrs == NULL)
	{
		ft_putstr_fd("cub3d: malloc issue.\n", 2);
		exit(1);
	}
	data->map = malloc(sizeof(char *) * 1);
	if (data->map == NULL)
	{
		ft_putstr_fd("cub3d: malloc issue.\n", 2);
		exit(1);
	}
	data->map[0] = NULL;
	data->txtrs[0] = NULL;
	data->txtrs[1] = NULL;
	data->txtrs[2] = NULL;
	data->txtrs[3] = NULL;
}

void	init_data(t_data *data)
{
	data->player.pos.x = -1;
	data->player.pos.y = -1;
	init_data_h(data);
}

void	print_data(t_data data)
{
	int i;

	printf("\n___TEXTURES_PATHS:___\n");
	i = 0;
	while (i < 4)
	{
		if (data.txtrs[i])
			printf("%s\n", data.txtrs[i]);
		i++;
	}
	
	printf("\n___COLORS:___\n");
	printf("  sky_colors = %i | %i | %i \n", ((char*)&(data.setup.sky_color))[0], ((char*)&(data.setup.sky_color))[1], ((char*)&(data.setup.sky_color))[2]);
	printf("floor_colors = %i | %i | %i \n", ((char*)&(data.setup.floor_color))[0], ((char*)&(data.setup.floor_color))[1], ((char*)&(data.setup.floor_color))[2]);

	printf("\n___PLAYER:___\n");
	printf("player   position: [%f][%f]\n", data.player.pos.x, data.player.pos.y);
	printf("player  direction: [%f][%f]\n", data.player.dir.x, data.player.dir.y);
	printf("player cam vector: [%f][%f]\n", data.player.cam.x, data.player.cam.y);

	printf("\n___MAP:___\n");
	i = 0;
	while (data.map[i])
	{
		printf("<%s>\n", data.map[i]);
		i++;
	}
}
