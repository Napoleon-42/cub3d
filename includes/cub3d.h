#ifndef CUB3D_H
# define CUB3D_H

/*######################################################*/
/*						INLCUDES						*/
/*######################################################*/

# include <math.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>

# include "math.h"

# include "keycodes_linux.h"
# include "2d_ftmath.h"
# include "libft.h"
# include "mlx.h"

/*######################################################*/
/*						DEFINES							*/
/*######################################################*/

# define NORTH_T 0
# define SOUTH_T 1 
# define EAST_T 2
# define WEST_T 3

# define WIDTH 1280
# define HEIGHT 720

# define _PI_ 3.14159265358979323846

/*######################################################*/
/*						STRUCTS							*/
/*######################################################*/

typedef	struct	s_setup
{
	int bpp;
	int size_line;
	int endian;
	int	floor_color;
	int	sky_color;
}				t_setup;

typedef	struct	s_player
{
	t_2point	pos;
	t_2vector	dir;
	t_2vector	cam;
}				t_player;

typedef struct	s_render
{
	t_2vector	player_dir;
	t_2point	player_pos;
	t_2point	camera;
	t_2vector	ray_dir;
	t_2point	map;
	t_2vector	side_dist;
	t_2vector	delta_dist;
	t_2vector	step;
	int			side;
	bool		hit;
	t_setup		*setup;
	t_player	*playr;
}				t_render;

typedef	struct	s_textures
{
	void	*north;
	void	*south;
	void	*est;
	void	*west;
}				t_textures;

typedef struct	s_cub3d
{
	char			**map;
	char			**txtrs;
	void			*mlx;
	void			*win;
	void			*img;
	char			*img_p;
	t_textures		texture;
	t_setup			setup;
	t_player		player;
}				t_data;

/*######################################################*/
/*						FUNCTIONS						*/
/*######################################################*/

/*	MAIN.C				*/

/* KEY_HOOKS.C			*/

int		key_hooks(int keycode, t_data *data);

/*	MAIN_UTILS.C		*/

int		starter(int ac, char **av, t_data *data);
int		check_file_name(char *file_name);
void	init_data_h(t_data *data);
void	init_data(t_data *data);
void	print_data(t_data data);

/*	EXITS.C				*/

int		ret_error(char *str, char ret_value);
void	free_mlx(t_data *data);
int		ft_exit(t_data *data);

/*	ADD_TEXTURES.C		*/

int		add_north_texture(t_data *data, char *temp);
int		add_south_texture(t_data *data, char *temp);
int		add_east_texture(t_data *data, char *temp);
int		add_west_texture(t_data *data, char *temp);
int		get_textures(t_data *data);

/*	PARSING.C			*/

void	exit_parser(t_data *data, char **temp, int fd);
int		check_map_line(char *map_line);
int		add_map_line(t_data *data, char *line);
int		add_colors(t_data *data, char *temp);
void	parsing_helper(t_data *data, char *temp, int *ret);
void	parsing(t_data *data, int fd);

/*	MAP_PROCESSING		*/

char	*add_n_c(char *str1, int n);
int		expand_map(t_data *data);
int		check_node(t_data *data, int i, int j);
void	add_p_pos(t_data *data, int x, int y);
void	set_cam_vector(t_data *data);
int		map_processing(t_data *data);

/*	RENDER.C			*/

void	render(t_data *data);

#endif